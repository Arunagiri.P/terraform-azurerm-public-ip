variable "public_ip_name" {
    description = "(Required) Name of the public IP to be created"
    type        = string
}

variable "location" {
    description = "(Required) Location of the public IP to be created"
    type        = string
  }

variable "resource_group_name" {
    description = "(Required) Resource group of the public IP to be created"
    type        = string
  }

variable "tags" {
    description = "(Required) Tags to be applied to the IP address to be created"
    type        = map(string)
    default     = {}
  }

variable "ip_addr" {
    description = "(Optional) Object with the settings for public IP deployment"
    type        = string
  }
  
variable "allocation_method" {
    description = "(Required) Defines the allocation method for this IP address. Possible values are Static or Dynamic."
    type        = string
    default = "Static"
  }

variable "zones" {
    description = "(Optional) A collection containing the availability zone to allocate the Public IP in. Changing this forces a new resource to be created."
    type        = list(any)
    default     = [1]
  }

variable "domain_name_label" {
    description = "(Optional) Label for the Domain Name. Will be used to make up the FQDN. If a domain name label is specified, an A DNS record is created for the public IP in the Microsoft Azure DNS system."
    type        = string
  }

variable "idle_timeout_in_minutes" {
    description = "(Optional) Specifies the timeout for the TCP idle connection. The value can be set between 4 and 30 minutes."
    type        = number 
  }

variable "ip_version" {
    description = "(Optional) The IP Version to use, IPv6 or IPv4. Changing this forces a new resource to be created. Defaults to IPv4"
    type        = string
    default     = "IPv4"
  }
  
variable "sku" {
    description = "(Optional) The SKU of the Public IP. Accepted values are Basic and Standard. Defaults to Basic"
    type        = string
    default     = "Standard"
  }

variable "sku_tier" {
    description = "(Optional) The SKU Tier that should be used for the Public IP. Possible values are Regional and Global. Defaults to Regional."
    type        = string
    default     = "Regional"
  }

# diagnostics settings object example for public ip  object
# diagnostics_settings = {
#     log = [
#                 #["Category name",  "Diagnostics Enabled(true/false)", "Retention Enabled(true/false)", Retention_period]
#                 ["DDoSProtectionNotifications", true, true, 30],
#                 ["DDoSMitigationFlowLogs", true, true, 30],
#                 ["DDoSMitigationReports", true, true, 30],
#         ]
#     metric = [
#                ["AllMetrics", true, true, 30],
#     ]
# }

 
# Example of ip_addr configuration object
# ip_addr = {
#       name                = "pip_test"
#       location            = "southeastasia"
#       rg                  = "uqvh-hub-egress-net"
#       allocation_method   = "Static"
#       #Dynamic Public IP Addresses aren't allocated until they're assigned to a resource (such as a Virtual Machine or a Load Balancer) by design within Azure

#       #properties below are optional
#       sku                 = "Standard"                        #defaults to Basic
#       ip_version          = "IPv4"                            #defaults to IP4, Only dynamic for IPv6, Supported arguments are IPv4 or IPv6, NOT Both
#       dns_prefix          = "arnaudmytestdeeee"
#       timeout             = 15                                #TCP timeout for idle connections. The value can be set between 4 and 30 minutes.
#       zones               = [1]                               #1 zone number, IP address must be standard, ZoneRedundant argument is not supported in provider at time of writing
#       #reverse_fqdn        = ""
#       #public_ip_prefix_id = "/subscriptions/783438ca-d497-4350-aa36-dc55fb0983ab/resourceGroups/uqvh-hub-ingress-net/providers/Microsoft.Network/publicIPPrefixes/myprefix"
#       #refer to the prefix and check sku types are same in IP and prefix
# }

