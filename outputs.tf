output "public_ip_id" {
  description = "The ID of the public ip."
  value = azurerm_public_ip.main.id
}

output "public_ip_address" {
  description = "The value of the public ip address"  
  value = azurerm_public_ip.main.ip_address
}
