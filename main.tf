resource "azurerm_public_ip" "main" {
  #name                = "public_ip"
  name                    = var.public_ip_name
  location                = var.location
  resource_group_name     = var.resource_group_name
  tags                    = merge({ "ResourceName" = lower("${var.public_ip_name}") }, var.tags, )
  allocation_method       = var.allocation_method
  zones                   = var.zones
  domain_name_label       = var.domain_name_label
  idle_timeout_in_minutes = var.idle_timeout_in_minutes
  ip_version              = var.ip_version
  sku                     = var.sku
  sku_tier                = var.sku_tier
}
